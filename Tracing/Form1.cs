﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.Numerics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tracing.Core;
using Tracing.Shapes;

namespace Tracing
{
    public partial class Form1 : Form
    {
        private CancellationTokenSource _cancellationTokenSource;
        private static Scene _scene;

        public Form1()
        {

            var s = new Settings();
            Settings.Instance = s;

            InitializeComponent();
            cancelBtn.Enabled = false;
            KeyPreview = true;
            propertyGrid1.CommandsVisibleIfAvailable = true;
            propertyGrid1.SelectedObject = Settings.Instance;
            this.KeyDown += (sender, args) =>
            {
                if (args.KeyCode == Keys.F12)
                {
                    this.panel1.Visible = !panel1.Visible;
                } else if (args.KeyCode == Keys.F5)
                {
                    StartGeneration();
                }
            };
            this.cancelBtn.Click += async (sender, args) =>
            {
                if (_cancellationTokenSource != null)
                {
                    generateBtn.Enabled = true;
                    cancelBtn.Enabled = false;
                    _cancellationTokenSource.Cancel();
                    
                }
            };
            this.generateBtn.Click += (sender, args) => { StartGeneration(); };
        }

        private async void StartGeneration()
        {
            if (_cancellationTokenSource != null) { }
            else
            {
                this.pictureBox1.Image = null;
                generateBtn.Enabled = false;
                cancelBtn.Enabled = true;
                Progress<float> progress = new Progress<float>();
                float acc = 0;
                int tileCount = 0;
                Stopwatch swp = Stopwatch.StartNew();
                progress.ProgressChanged += (o, f) =>
                {
                    acc += f;
                    tileCount++;
                    var elapsed = swp.ElapsedMilliseconds;
#if PROGRESSIVE
                    if (elapsed > 1000)
                    {
                        swp = Stopwatch.StartNew();
                        pictureBox1.Image = _scene.Camera.Film.CreateImage();
                        Application.DoEvents();
                    }
#endif
                    if (acc > 1)
                    {
                        progressBar1.Value = (int) Math.Min(100, Math.Ceiling(acc));
                    }
                };
                _cancellationTokenSource = new CancellationTokenSource();

                label1.Text = "Generating";
                Stopwatch sw = Stopwatch.StartNew();
                Image img = await Generate(_cancellationTokenSource.Token, progress);
                var time = sw.ElapsedMilliseconds;
                _cancellationTokenSource = null;
                label1.Text = $"{time}ms";
                //this.ClientSize = img.Size;
                this.pictureBox1.Image = img;
                img.Save("output.png", ImageFormat.Png);

                generateBtn.Enabled = true;
                cancelBtn.Enabled = false;
            }
        }

        private static Task<Image> Generate(CancellationToken token, IProgress<float> progress)
        {
            Settings instance = Settings.Instance;

            Material grey   = new Material { Color = new Vector3(0.7f, 0.7f, 0.7f) };
            Material red   = new Material { Color = new Vector3(1, 0, 0) };
            Material blue  = new Material { Color = new Vector3(0, 0, 1) };
            Material green = new Material { Color = new Vector3(0, 1, 0) };

            _scene = new Scene
            {
                Camera = new Camera(instance.LensRadius, instance.FocalDistance, instance.Fov),
                Objects = new List<IObject>
                {
                    new Shapes.Plane(Vector3.UnitX, -Vector3.UnitX*5, red),
                    new Shapes.Plane(-Vector3.UnitX, Vector3.UnitX*5, green),
                    new Shapes.Plane(-Vector3.UnitZ, Vector3.UnitZ*55, blue),
                    new Shapes.Plane(-Vector3.UnitY, Vector3.UnitY*5, grey),
                    new Shapes.Plane(Vector3.UnitY, -Vector3.UnitY*5, grey),
                    //new Sphere(Vector3.UnitZ * 3 - Vector3.UnitX, 1f, red),
                    new Sphere(Vector3.UnitZ * 22 + Vector3.UnitX * 2, 1f, green),
                    new Sphere(Vector3.UnitZ * 35 + Vector3.UnitY * -3, 2.5f, blue),
                }
            };
            return Task.Run(() =>
            {
                return new WhittedIntegrator().Render(instance, _scene, token, progress);
            });
        }
    }
}
