﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Numerics;
using Tracing.Core.Maths;
using Tracing.Shapes;

namespace Tracing.Core {
    public class VisibilityTester {
        public bool Unoccluded(Scene scene)
        {
            return true;
        }
    }
    public class Scene : IObject
    {
        public Camera Camera { get; set; }
        public List<IObject> Objects;
        public List<ILight> Lights;
        public bool Intersects(in Ray r, out Intersection closestInt)
        {
            closestInt = new Intersection { DistanceSquared = Single.MaxValue };
            bool found = false;
            foreach (IObject o in Objects)
            {
                if (o.Intersects(in r, out Intersection intersection))
                {
                    if (intersection.DistanceSquared < closestInt.DistanceSquared)
                    {
                        closestInt = intersection;
                        found = true;
                    }
                }
            }

            return found;
        }

        public Material Material { get; }
    }
}