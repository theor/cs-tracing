﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Numerics;

namespace Tracing.Core {
    public class PixelSampler : Sampler
    {
        protected readonly int _nSampledDimensions;
        protected List<float[]> samples1D;
        protected List<Vector2[]> samples2D;
        protected int current1DDimension = 0;
        protected int current2DDimension = 0;
        protected RNG rng = new RNG();

        public PixelSampler(int samplesPerPixel, int nSampledDimensions) : base(samplesPerPixel)
        {
            _nSampledDimensions = nSampledDimensions;
            samples1D = new List<float[]>(nSampledDimensions);
            samples2D = new List<Vector2[]>(nSampledDimensions);
            for (int i = 0; i < nSampledDimensions; ++i)
            {
                samples1D.Add(new float[samplesPerPixel]);
                samples2D.Add(new Vector2[samplesPerPixel]);
            }
        }

        public override bool StartNextSample()
        {

            current1DDimension = current2DDimension = 0;
            return base.StartNextSample();
        }

        public override bool SetSampleNumber(int sampleNum)
        {

            current1DDimension = current2DDimension = 0;
            return base.SetSampleNumber(sampleNum);
        }

        public override float Get1D()
        {
            Debug.Assert(currentPixelSampleIndex < samplesPerPixel);
            if (current1DDimension < samples1D.Count)
                return samples1D[current1DDimension++][currentPixelSampleIndex];
            else
                return rng.UniformFloat();
        }

        public override Vector2 Get2D()
        {
            Debug.Assert(currentPixelSampleIndex < samplesPerPixel);
            if (current2DDimension < samples2D.Count)
                return samples2D[current2DDimension++][currentPixelSampleIndex];
            else
                return new Vector2(rng.UniformFloat(), rng.UniformFloat());
        }

        public override Sampler Clone(int seed)
        {
            var pixelSampler = new PixelSampler(samplesPerPixel, _nSampledDimensions);
            pixelSampler.rng.SetSequence((ulong) seed);
            return pixelSampler;
        }

        //public override Sampler Clone(int seed)
        //{
        //    return new PixelSampler(samplesPerPixel, );
        //}
    }
}