﻿using System;
using System.Drawing;
using System.Numerics;

namespace Tracing.Core {
    class ZeroTwoSequenceSampler : PixelSampler
    {
        public ZeroTwoSequenceSampler(int samplesPerPixel, int nSampledDimensions = 4)
            : base(RoundUpPow2(samplesPerPixel), nSampledDimensions) { }

        public override Sampler Clone(int seed)
        {
            ZeroTwoSequenceSampler clone = new ZeroTwoSequenceSampler(samplesPerPixel, _nSampledDimensions);
            clone.rng.SetSequence((ulong) seed);
            return clone;
        }

        public override int RoundCount(int count)
        {
            return RoundUpPow2(count);
        }

        private static int RoundUpPow2(int v)
        {
            v--;
            v |= v >> 1;
            v |= v >> 2;
            v |= v >> 4;
            v |= v >> 8;
            v |= v >> 16;
            return v + 1;
        }

        public unsafe override void StartPixel(in Point p)
        {
            // Generate 1D and 2D pixel sample components using $(0,2)$-sequence
            for (int i = 0; i < samples1D.Count; ++i)
            {
                float[] floats = samples1D[i];
                fixed(float* samples = &floats[0])
                    VanDerCorput(1, samplesPerPixel, samples, rng);
            }
            for (int i = 0; i < samples2D.Count; ++i)
                fixed(Vector2* pSamples2D = &samples2D[i][0])
                    Sobol2D(1, samplesPerPixel, pSamples2D, rng);

            // Generate 1D and 2D array samples using $(0,2)$-sequence
            //for (int i = 0; i < samples1DArraySizes.Count; ++i)
            //    VanDerCorput(samples1DArraySizes[i], samplesPerPixel,
            //        sampleArray1D[i], rng);
            //for (int i = 0; i < samples2DArraySizes.Count; ++i)
            //    Sobol2D(samples2DArraySizes[i], samplesPerPixel, sampleArray2D[i][0],
            //        rng);
            base.StartPixel(in p);
        }

        private static readonly uint[][] CSobol =
        {
            new uint[]
            {
                0x80000000, 0x40000000, 0x20000000, 0x10000000, 0x8000000, 0x4000000,
                0x2000000, 0x1000000, 0x800000, 0x400000, 0x200000, 0x100000, 0x80000,
                0x40000, 0x20000, 0x10000, 0x8000, 0x4000, 0x2000, 0x1000, 0x800,
                0x400, 0x200, 0x100, 0x80, 0x40, 0x20, 0x10, 0x8, 0x4, 0x2, 0x1
            },
            new uint[]
            {
                0x80000000, 0xc0000000, 0xa0000000, 0xf0000000, 0x88000000, 0xcc000000,
                0xaa000000, 0xff000000, 0x80800000, 0xc0c00000, 0xa0a00000, 0xf0f00000,
                0x88880000, 0xcccc0000, 0xaaaa0000, 0xffff0000, 0x80008000, 0xc000c000,
                0xa000a000, 0xf000f000, 0x88008800, 0xcc00cc00, 0xaa00aa00, 0xff00ff00,
                0x80808080, 0xc0c0c0c0, 0xa0a0a0a0, 0xf0f0f0f0, 0x88888888, 0xcccccccc,
                0xaaaaaaaa, 0xffffffff
            }
        };
        private unsafe void Sobol2D(int nSamplesPerPixelSample, int nPixelSamples, Vector2* samples, RNG rng)
        {
            Point scramble = new Point(
                rng.UniformInt32(),
                rng.UniformInt32());
            fixed(uint* pCSobol0 = &CSobol[0][0])
            fixed(uint* pCSobol1 = &CSobol[1][0])
            {
                GrayCodeSample(pCSobol0, pCSobol1, nSamplesPerPixelSample * nPixelSamples,
                    scramble, samples);

            }
            for (int i = 0; i < nPixelSamples; ++i)
                Shuffle(samples + i * nSamplesPerPixelSample, nSamplesPerPixelSample, 1,
                    rng);
            Shuffle(samples, nPixelSamples, nSamplesPerPixelSample, rng);
        }

        private static readonly uint[] CVanDerCorput =
        {
            0x80000000, 0x40000000, 0x20000000, 0x10000000, 0x8000000, 0x4000000,
            0x2000000, 0x1000000, 0x800000, 0x400000, 0x200000, 0x100000,
            0x80000, 0x40000, 0x20000, 0x10000, 0x8000, 0x4000,
            0x2000, 0x1000, 0x800, 0x400, 0x200, 0x100,
            0x80, 0x40, 0x20, 0x10, 0x8, 0x4,
        };

        private unsafe void VanDerCorput(int nSamplesPerPixelSample, int nPixelSamples, float* samples, RNG rng)
        {
            uint scramble = rng.UniformUInt32();
            int totalSamples = nSamplesPerPixelSample * nPixelSamples;
            fixed (uint* c = CVanDerCorput)
                GrayCodeSample(c, totalSamples, scramble, samples);
            // Randomly shuffle 1D sample points
            for (int i = 0; i < nPixelSamples; ++i)
                Shuffle(samples + i * nSamplesPerPixelSample, nSamplesPerPixelSample, 1,
                    rng);
            Shuffle(samples, nPixelSamples, nSamplesPerPixelSample, rng);
        }

        private static unsafe void Shuffle(Vector2* samp, int count, int nDimensions, RNG rng)
        {
            for (int i = 0; i < count; ++i)
            {
                int other = i + rng.UniformInt32((uint) (count - i));
                for (int j = 0; j < nDimensions; ++j)
                {
                    var tmp = samp[nDimensions * i + j];
                    samp[nDimensions * i + j] = samp[nDimensions * other + j];
                    samp[nDimensions * other + j] = tmp;
                }
            }
        }

        private static unsafe void Shuffle(float* samp, int count, int nDimensions, RNG rng)
        {
            for (int i = 0; i < count; ++i)
            {
                int other = i + rng.UniformInt32((uint) (count - i));
                for (int j = 0; j < nDimensions; ++j)
                {
                    var tmp = samp[nDimensions * i + j];
                    samp[nDimensions * i + j] = samp[nDimensions * other + j];
                    samp[nDimensions * other + j] = tmp;
                }
            }
        }

        public const float OneMinusEpsilon = 0.99999994f;

        private static unsafe void GrayCodeSample(uint* C, int n, uint scramble, float* p)
        {
            uint v = scramble;
            for (uint i = 0; i < n; ++i)
            {
                p[i] = Math.Min(v * 2.3283064365386963e-10f /* 1/2^32 */,
                    OneMinusEpsilon);
                v ^= C[CountTrailingZeros(i + 1)];
            }
        }

        private static unsafe void GrayCodeSample(uint* C0, uint* C1, int n, Point scramble, Vector2* p)
        {
            uint[] v = { (uint)scramble.X, (uint)scramble.Y };
            for (uint i = 0; i < n; ++i)
            {
                p[i].X = Math.Min(v[0] * (2.3283064365386963e-10f), OneMinusEpsilon);
                p[i].Y = Math.Min(v[1] * (2.3283064365386963e-10f), OneMinusEpsilon);

                v[0] ^= C0[CountTrailingZeros(i + 1)];
                v[1] ^= C1[CountTrailingZeros(i + 1)];
            }
        }

        public static int CountTrailingZeros(uint i)
        {
            // HD, Figure 5-14
            uint y;
            if (i == 0) return 32;
            int n = 31;
            y = i << 16;
            if (y != 0)
            {
                n = n - 16;
                i = y;
            }

            y = i << 8;
            if (y != 0)
            {
                n = n - 8;
                i = y;
            }

            y = i << 4;
            if (y != 0)
            {
                n = n - 4;
                i = y;
            }

            y = i << 2;
            if (y != 0)
            {
                n = n - 2;
                i = y;
            }

            return (int) (n - ((i << 1) >> 31));
        }
    }
}