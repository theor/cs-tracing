﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Numerics;
using Tracing.Core.Maths;

namespace Tracing.Core {
    public abstract class Sampler
    {
        // Sampler Interface
        protected Sampler(int samplesPerPixel)
        {
            this.samplesPerPixel = samplesPerPixel;
        }

        public abstract float Get1D();

        public abstract Vector2 Get2D();

        public abstract Sampler Clone(int seed);

        public virtual int RoundCount(int n) { return n; }

        public CameraSample GetCameraSample(in Point pRaster)
        {
            CameraSample cs = new CameraSample();
            cs.pFilm = pRaster.ToVector2() + Get2D();
            cs.time = Get1D();
            cs.pLens = Get2D();
            return cs;
        }

        public virtual void StartPixel(in Point p)
        {
            currentPixel = p;
            currentPixelSampleIndex = 0;

            // Reset array offsets for next pixel sample
            array1DOffset = array2DOffset = 0;
        }

        public virtual bool StartNextSample()
        {
            // Reset array offsets for next pixel sample
            array1DOffset = array2DOffset = 0;

            return ++currentPixelSampleIndex < samplesPerPixel;
        }

        public virtual bool SetSampleNumber(int sampleNum)
        {
            // Reset array offsets for next pixel sample
            array1DOffset = array2DOffset = 0;

            currentPixelSampleIndex = sampleNum;
            return currentPixelSampleIndex < samplesPerPixel;
        }

        public void Request1DArray(int n)
        {
            Debug.Assert(RoundCount(n) == n);
            samples1DArraySizes.Add(n);
            sampleArray1D.Add(new List<float>(n * samplesPerPixel));
        }

        public void Request2DArray(int n)
        {
            Debug.Assert(RoundCount(n) == n);
            samples2DArraySizes.Add(n);
            sampleArray2D.Add(new List<Vector2>(n * samplesPerPixel));
        }

        public IEnumerable<float> Get1DArray(int n)
        {
            if (array1DOffset == sampleArray1D.Count)
                return null;
            Debug.Assert(samples1DArraySizes[array1DOffset] == n);
            Debug.Assert(currentPixelSampleIndex < samplesPerPixel);
            return sampleArray1D[array1DOffset++].Skip(currentPixelSampleIndex * n).Take(n);
        }

        public IEnumerable<Vector2> Get2DArray(int n)
        {
            if (array2DOffset == sampleArray2D.Count)
                return null;
            Debug.Assert(samples2DArraySizes[array2DOffset] == n);
            Debug.Assert(currentPixelSampleIndex < samplesPerPixel);
            return sampleArray2D[array2DOffset++].Skip(currentPixelSampleIndex * n).Take(n);
        }

        public string StateString()
        {
            return $"({currentPixel.X},{currentPixel.Y}), sample {currentPixelSampleIndex}";
        }
        public long CurrentSampleNumber => currentPixelSampleIndex;

        // Sampler Public Data
        public int samplesPerPixel;


        // Sampler Protected Data
        protected Point currentPixel;
        protected int currentPixelSampleIndex;
        protected List<int> samples1DArraySizes, samples2DArraySizes;
        protected List<List<float>> sampleArray1D;
        protected List<List<Vector2>> sampleArray2D;

        // Sampler Private Data
        private int array1DOffset;
        private int array2DOffset;
    }
}