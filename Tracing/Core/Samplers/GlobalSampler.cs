﻿using System.Drawing;
using System.Numerics;

namespace Tracing.Core {
    public abstract class GlobalSampler : Sampler
    {
        // GlobalSampler Public Methods
        public override bool StartNextSample()
        {
            dimension = 0;
            intervalSampleIndex = GetIndexForSample(currentPixelSampleIndex + 1);
            return base.StartNextSample();
        }
        public override void StartPixel(in Point p)
        {
            base.StartPixel(in p);
        }

        public override bool SetSampleNumber(int sampleNum)
        {
            dimension = 0;
            intervalSampleIndex = GetIndexForSample(sampleNum);
            return base.SetSampleNumber(sampleNum);
        }

        public override float Get1D()
        {
            if (dimension >= arrayStartDim && dimension < arrayEndDim)
                dimension = arrayEndDim;
            return SampleDimension(intervalSampleIndex, dimension++);
        }

        public override Vector2 Get2D()
        {
            if (dimension + 1 >= arrayStartDim && dimension < arrayEndDim)
                dimension = arrayEndDim;
            Vector2 p = new Vector2(
                SampleDimension(intervalSampleIndex, dimension),
                SampleDimension(intervalSampleIndex, dimension + 1)
            );
            dimension += 2;
            return p;
        }

        public GlobalSampler(int samplesPerPixel) : base(samplesPerPixel) { }

        public abstract float SampleDimension(int index, int dimension);
        public abstract int GetIndexForSample(int sampleNum);


        // GlobalSampler Private Data
        private int dimension;
        private int intervalSampleIndex;
        private const int arrayStartDim = 5;
        private int arrayEndDim;
    };
}