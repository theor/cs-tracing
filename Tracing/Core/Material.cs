﻿using System.Numerics;
using Tracing.Core.Maths;

namespace Tracing.Core {
    public class Material
    {
        public Vector3 Color = Vector3.One;

        public Spectrum Shading(ref Intersection intersection)
        {
            if (Settings.Instance.ShowNormals)
            {
                Vector3 n = (intersection.ContactNormal + Vector3.One) / 2;
                return new Spectrum(n);
            }

            float dot = Vector3.Dot(Vector3.UnitY, intersection.ContactNormal);
            float ambient = 0.1f;
            float diffuse = ambient + (dot <= 0 ? 0 : dot * (1-ambient));
            return new Spectrum(Color * new Vector3(diffuse, diffuse, diffuse));
        }
    }
}