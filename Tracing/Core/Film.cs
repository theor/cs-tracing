﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Numerics;
using System.Threading;
using Microsoft.Win32.SafeHandles;
using Tracing.Core.Maths;

namespace Tracing.Core
{
    public class Film
    {
        public struct Pixel
        {
            public Spectrum xyz;
            public float filterWeightSum;
            public Vector3 splatXYZ;

            public override string ToString()
            {
                return $"{nameof(xyz)}: {xyz}, {nameof(filterWeightSum)}: {filterWeightSum}";
            }
        };

        private Mutex mutex = new Mutex(false, Guid.NewGuid().ToString());

        private float[] filterTable;

        const int filterTableWidth = 16;
        public Size Resolution { get; }
        public Filter Filter { get; }
        public float Diagonal { get; }
        public string Filename { get; }
        public float Scale { get; }
        public float MaxSampleLuminance { get; }
        public Bounds<Point> CroppedPixelBounds { get; }

        public Pixel[] Pixels;

        public Film(Size resolution, in Bounds<Vector2> cropWindow,
            Filter filter, float diagonal,
            string filename, float scale = 1.0f, float maxSampleLuminance = Single.PositiveInfinity)
        {
            Resolution = resolution;
            Filter = filter;
            Diagonal = diagonal * 0.001f;
            Filename = filename;
            Scale = scale;
            MaxSampleLuminance = maxSampleLuminance;
            CroppedPixelBounds = new Bounds<Point>(
                new Point( 
                (int)Math.Ceiling((double)resolution.Width * cropWindow.Min.X),
                (int)Math.Ceiling((double)resolution.Height * cropWindow.Min.Y)),
                new Point((int)Math.Ceiling((double)resolution.Width * cropWindow.Max.X),
                (int)Math.Ceiling((double)resolution.Height * cropWindow.Max.Y)));
            Pixels = new Pixel[CroppedPixelBounds.Area()];

            int offset = 0;
            filterTable = new float[filterTableWidth * filterTableWidth];
            for (int y = 0; y < filterTableWidth; ++y)
            {
                for (int x = 0; x < filterTableWidth; ++x, ++offset)
                {
                    Vector2 p = new Vector2(
                        (x + 0.5f) * filter.Radius.X / filterTableWidth,
                        (y + 0.5f) * filter.Radius.Y / filterTableWidth);
                    filterTable[offset] = filter.Evaluate(p);
                }
            }
        }

        public FilmTile GetFilmTile(Bounds<Point> tileBounds)
        {
            var bounds = new Bounds<Point>(
                new Point((int)Math.Ceiling(tileBounds.Min.X - 0.5f - Filter.Radius.X),
                    (int)Math.Ceiling(tileBounds.Min.Y - 0.5f - Filter.Radius.Y)),
                new Point((int)Math.Floor(tileBounds.Max.X - 0.5f + Filter.Radius.X),
                    (int)Math.Floor(tileBounds.Max.Y - 0.5f + Filter.Radius.Y))
            );
            Bounds<Point> tilePixelBounds = Bounds<Point>.Intersect(bounds, CroppedPixelBounds);
            return new FilmTile(tilePixelBounds, Filter.Radius, filterTable, filterTableWidth, MaxSampleLuminance);
        }

        public Bounds<Point> GetSampleBounds()
        {
            return new Bounds<Point>(
                new Point(
                    (int)Math.Floor(CroppedPixelBounds.Min.X + 0.5f - Filter.Radius.X),
                    (int)Math.Floor(CroppedPixelBounds.Min.Y + 0.5f - Filter.Radius.Y)
                ),
                new Point(
                    (int)Math.Ceiling(CroppedPixelBounds.Max.X- 0.5f + Filter.Radius.X),
                    (int)Math.Ceiling(CroppedPixelBounds.Max.Y - 0.5f + Filter.Radius.Y)
                )
            );
        }

        public void MergeFilmTile(in FilmTile tile)
        {
            //return;
            mutex.WaitOne();
            {
                foreach (Point pixel in tile.tilePixelBounds.EnumeratePixels())
                {
                    // Merge _pixel_ into _Film::pixels_
                    ref FilmTilePixel tilePixel = ref tile.GetPixel(pixel.X, pixel.Y);
                    ref Pixel mergePixel = ref GetPixel(pixel.X, pixel.Y);
                    mergePixel.xyz += tilePixel.contribSum;
                    //float[] xyz = new float[3];
                    //tilePixel.contribSum.ToXYZ(xyz);
                    //for (int i = 0; i < 3; ++i)
                    //    mergePixel.xyz[i] += xyz[i];
                    mergePixel.filterWeightSum += tilePixel.filterWeightSum;
                }
            }
            mutex.ReleaseMutex();
        }

        private ref Pixel GetPixel(int x, int y)
        {
            int width = CroppedPixelBounds.Max.X - CroppedPixelBounds.Min.X;
            int offset = (x - CroppedPixelBounds.Min.X) +
                         (y - CroppedPixelBounds.Min.Y) * width;
            return ref Pixels[offset];
        }

        public unsafe Bitmap CreateImage()
        {
            Bitmap img = new Bitmap(CroppedPixelBounds.Width(), CroppedPixelBounds.Height(), PixelFormat.Format24bppRgb);
            //Bitmap img = new Bitmap(Resolution.Width, Resolution.Height, PixelFormat.Format32bppArgb);
            BitmapData data = img.LockBits(new Rectangle(Point.Empty, img.Size), ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);

            //{
            //    Action<int, int, Vector3> a = (x, y, c) =>
            //    {
            //        ref Pixel pixel = ref GetPixel(x, y);
            //        pixel.xyz = c;
            //        pixel.filterWeightSum++;

            //    };

            //    for (int i = 0; i < CroppedPixelBounds.Width(); i++)
            //    {
            //        a(i, 0, Vector3.UnitX);
            //        a(i, 1, Vector3.UnitY);
            //        a(i, 2, Vector3.UnitZ);

            //    }
            //}

            //float[] rgb;//(new float[3 * CroppedPixelBounds.Area()]);
            byte* scan0 = (byte*) data.Scan0;
            int offset = 0;
            mutex.WaitOne();
            foreach (var p in CroppedPixelBounds.EnumeratePixels())
            {

                // Convert pixel XYZ color to RGB
                ref Pixel pixel = ref GetPixel(p.X, p.Y);
                //XYZToRGB(pixel.xyz, &rgb[3 * offset]);
                var rgb = pixel.xyz;
                // Normalize pixel with weight sum
                float filterWeightSum = pixel.filterWeightSum;

                if (filterWeightSum != 0)
                {
                    float invWt = 1 / filterWeightSum;

                    Vector3 weighted = new Vector3(
                        Math.Max(0, rgb.RGB.X * invWt),
                        Math.Max(0, rgb.RGB.Y * invWt),
                        Math.Max(0, rgb.RGB.Z * invWt)
                    );
                        

                    *(scan0 + 3 * offset + 0) = (byte)Clamp((int)(weighted.Z * 255.0), 0, 256);
                    *(scan0 + 3 * offset + 1) = (byte)Clamp((int)(weighted.Y * 255.0), 0, 256);
                    *(scan0 + 3 * offset + 2) = (byte)Clamp((int)(weighted.X * 255.0), 0, 256);
                }

                //// Add splat value at pixel
                //float splatRGB[3];
                //float splatXYZ[3] = {pixel.splatXYZ[0], pixel.splatXYZ[1],
                //    pixel.splatXYZ[2]};
                //XYZToRGB(splatXYZ, splatRGB);
                //rgb[3 * offset] += splatScale * splatRGB[0];
                //rgb[3 * offset + 1] += splatScale * splatRGB[1];
                //rgb[3 * offset + 2] += splatScale * splatRGB[2];

                //// Scale pixel value by _scale_
                //rgb[3 * offset] *= scale;
                //rgb[3 * offset + 1] *= scale;
                //rgb[3 * offset + 2] *= scale;
                ++offset;
            }
            mutex.ReleaseMutex();

            img.UnlockBits(data);
            return img;
        }
        private static int Clamp(int v, int a, int b)
        {
            return v < a ? a : (v >= b ? (b - 1) : v);
        }
    }

    public struct FilmTile
    {
        public Bounds<Point> tilePixelBounds;
        private Vector2 filterRadius;
        private Vector2 invFilterRadius;
        private float[] filterTable;
        private int filterTableSize;
        private float maxSampleLuminance;
        FilmTilePixel[] pixels;

        public FilmTile(Bounds<Point> tilePixelBounds, Point radius, float[] filterTable, int filterTableSize, float maxSampleLuminance)
        {
            this.tilePixelBounds = tilePixelBounds;
            this.filterRadius = radius.ToVector2();
            this.invFilterRadius = new Vector2(1 / filterRadius.X, 1/ filterRadius.Y);
            this.filterTable = filterTable;
            this.filterTableSize = filterTableSize;
            this.maxSampleLuminance = maxSampleLuminance;
            pixels = new FilmTilePixel[Math.Max(0, tilePixelBounds.Area())];
        }

        public unsafe void AddSample(Vector2 pFilm, Spectrum l, float sampleWeight)
        {
            //if (l.y() > maxSampleLuminance)
            //    l *= maxSampleLuminance / l.y();
            // Compute sample's raster bounds
            Vector2 pFilmDiscrete = pFilm - new Vector2(0.5f, 0.5f);
            Point p0 = (pFilmDiscrete - filterRadius).ToPointCeiling();
            Point p1 = (pFilmDiscrete + filterRadius + Vector2.One).ToPointFloor();
            p0 = MathUtils.Max(p0, tilePixelBounds.Min);
            p1 = MathUtils.Min(p1, tilePixelBounds.Max);

            // Loop over filter support and add sample to pixel arrays

            // Precompute $x$ and $y$ filter table offsets
            int* ifx = stackalloc int[p1.X - p0.X];
            for (int x = p0.X; x < p1.X; ++x)
            {
                float fx = Math.Abs((x - pFilmDiscrete.X) * invFilterRadius.X *
                                    filterTableSize);
                ifx[x - p0.X] = Math.Min((int)Math.Floor(fx), filterTableSize - 1);
            }
            int* ify = stackalloc int[p1.Y - p0.Y];
            for (int y = p0.Y; y < p1.Y; ++y)
            {
                float fy = Math.Abs((y - pFilmDiscrete.Y) * invFilterRadius.Y *
                                    filterTableSize);
                ify[y - p0.Y] = Math.Min((int)Math.Floor(fy), filterTableSize - 1);
            }
            for (int y = p0.Y; y < p1.Y; ++y)
            {
                for (int x = p0.X; x < p1.X; ++x)
                {
                    // Evaluate filter value at $(x,y)$ pixel
                    int offset = ify[y - p0.Y] * filterTableSize + ifx[x - p0.X];
                    float filterWeight = filterTable[offset];

                    // Update pixel values with filtered sample contribution
                    ref FilmTilePixel pixel = ref GetPixel(x, y);
                    pixel.contribSum += l * sampleWeight * filterWeight;
                    pixel.filterWeightSum += filterWeight;
                }
            }
        }

        public ref FilmTilePixel GetPixel(int x, int y)
        {
            //De(InsideExclusive(p, pixelBounds));
            int width = tilePixelBounds.Max.X - tilePixelBounds.Min.X;
            int offset =
                (x - tilePixelBounds.Min.X) + (y - tilePixelBounds.Min.Y) * width;
            return ref pixels[offset];
        }
    }

    public static class MathUtils
    {
        public static Point Min(Point a, Point b)
        {
            return new Point(Math.Min(a.X, b.X), Math.Min(a.Y, b.Y));
        }
        public static Point Max(Point a, Point b)
        {
            return new Point(Math.Max(a.X, b.X), Math.Max(a.Y, b.Y));
        }

        public static float AbsDot(Vector3 wi, Vector3 vector3)
        {
            throw new NotImplementedException();
        }
    }

    public struct FilmTilePixel {
        public Spectrum contribSum;
        public float filterWeightSum;
    }
}