﻿using System.Numerics;
using Tracing.Shapes;

namespace Tracing.Core {
    public struct Intersection
    {
        public float DistanceSquared;
        public Vector3 ContactPoint;
        public Vector3 ContactNormal;
        public Vector3 wo;
        public IObject Object { get; set; }
    }
}