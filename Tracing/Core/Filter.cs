﻿using System;
using System.Drawing;
using System.Numerics;

namespace Tracing.Core {
    public abstract class Filter
    {
        public Point Radius => new Point(1, 1);
        public abstract float Evaluate(Vector2 p);
    }

    public class TriangleFilter : Filter
    {
        public override float Evaluate(Vector2 p)
        {
            return Math.Max(0f, Radius.X - Math.Abs(p.X)) *
                   Math.Max(0f, Radius.Y - Math.Abs(p.Y));
        }
    }
}