﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Numerics;
using System.Threading;
using System.Threading.Tasks;
using Tracing.Core.Maths;
using Tracing.Shapes;

namespace Tracing.Core
{
    public interface IIntegrator {
        Image Render(Settings settings, Scene scene, CancellationToken token, IProgress<float> progress);
    }

    public abstract class SamplerIntegrator : IIntegrator
    {
        public const int TileSize = 16;
        protected abstract Spectrum Li(in Ray ray, Scene scene, in Sampler tileSampler, int depth = 0);

        public Image Render(Settings settings, Scene scene, CancellationToken token, IProgress<float> progress)
        {
            Sampler sampler = new ZeroTwoSequenceSampler(settings.SamplesPerPixels, 4);

            Camera camera = scene.Camera;
            Bounds<Point> sampleBounds = camera.Film.GetSampleBounds();
            Point sampleExtent = sampleBounds.Diagonal();
            Point nTiles = new Point(
                (sampleExtent.X + TileSize - 1) / TileSize,
                (sampleExtent.Y + TileSize - 1) / TileSize);

            For(token, nTiles, tile =>
            {
                unsafe
                {
                    Intersection intersection = default;
                    int seed = tile.Y * nTiles.X + tile.X;
                    var tileSampler = sampler.Clone(seed);

                    int x0 = sampleBounds.Min.X + tile.X * TileSize;
                    int x1 = Math.Min(x0 + TileSize, sampleBounds.Max.X);
                    int y0 = sampleBounds.Min.Y + tile.Y * TileSize;
                    int y1 = Math.Min(y0 + TileSize, sampleBounds.Max.Y);
                    Bounds<Point> tileBounds = new Bounds<Point>(new Point(x0, y0), new Point(x1, y1));

                    FilmTile filmTile = camera.Film.GetFilmTile(tileBounds);

                    foreach (Point pixel in tileBounds.EnumeratePixels())
                    {
                        tileSampler.StartPixel(in pixel);
                        //if (!InsideExclusive(pixel, pixelBounds))
                        //  continue;
                        do
                        {
                            // Initialize _CameraSample_ for current sample
                            CameraSample cameraSample = tileSampler.GetCameraSample(in pixel);

                            // Generate camera ray for current sample
                            float rayWeight = camera.GetRay(in cameraSample, out Ray r);
                            // TODO differential
                            Spectrum l = Spectrum.Zero;

                            if (rayWeight > 0) l = Li(in r, scene, in tileSampler);
                  
                            filmTile.AddSample(cameraSample.pFilm, l, rayWeight);
                        } while (tileSampler.StartNextSample());
                    }

                    camera.Film.MergeFilmTile(in filmTile);
                }
            }, progress);

            Bitmap img = camera.Film.CreateImage();

            return img;
        }

        protected Vector4 SpecularReflect(in Ray ray, Intersection intersection, Scene scene, Sampler tileSampler, int depth)
        {
            return Vector4.Zero;
        }

        protected Vector4 SpecularTransmit(in Ray ray, Intersection intersection, Scene scene, Sampler tileSampler, int depth)
        {
            return Vector4.Zero;
        }

        private static void For(CancellationToken token, Point nTiles, Action<Point> f, IProgress<float> progress)
        {
            var progressUnit = 100.0f / (nTiles.X * nTiles.Y);
            if (Settings.Instance.Parallel)
            {
                Parallel.For(0, nTiles.Y, j =>
                {
                    if (token.IsCancellationRequested)
                        return;
                    for (int i = 0; i < nTiles.X; i++)
                    {
                        f(new Point(i, j));
                        progress.Report(progressUnit);
                    }
                });
            }
            else
            {
                for (int j = 0; j < nTiles.Y; j++)
                {
                    if (token.IsCancellationRequested)
                        return;
                    for (int i = 0; i < nTiles.X; i++)
                    {
                        f(new Point(i, j));
                        progress.Report(progressUnit);
                    }
                }
            }
        }
    }

    public class WhittedIntegrator : SamplerIntegrator
    {
        private int maxDepth = 2;

        protected override Spectrum Li(in Ray ray, Scene scene, in Sampler tileSampler, int depth = 0)
        {
            if (!scene.Intersects(in ray, out Intersection intersection))
            {
                return Spectrum.Zero;
            }

            //// Compute emitted and reflected light at ray intersection point

            //// Initialize common variables for Whitted integrator
            Vector3 n = intersection.ContactNormal;
            Vector3 wo = intersection.wo;

            //// Compute scattering functions for surface interaction
            //intersection.ComputeScatteringFunctions(ray, arena);
            //if (!intersection.bsdf)
            //    return Li(intersection.SpawnRay(ray.d), scene, sampler, arena, depth);

            //// Compute emitted light if ray hit an area light source
            //L += intersection.Le(wo);

            //Spectrum l = Spectrum.Zero;
            Spectrum l = intersection.Object.Material.Shading(ref intersection);
            //// Add contribution of each light source
            //foreach (ILight light in scene.Lights)
            //{
            //    Vector3 wi;
            //    float pdf;
            //    VisibilityTester visibility = new VisibilityTester();
            //    Spectrum Li =
            //        light.Sample_Li(intersection, tileSampler.Get2D(), out wi, out pdf, visibility);
            //    if (Li.IsBlack() || pdf == 0) continue;
            //    Spectrum f = intersection.bsdf.f(wo, wi);
            //    if (!f.IsBlack() && visibility.Unoccluded(scene))
            //        l += f * Li * MathUtils.AbsDot(wi, n) / pdf;
            //}
            //if (depth + 1 < maxDepth)
            //{
            //    // Trace rays for specular reflection and refraction
            //    l += SpecularReflect(in ray, intersection, scene, tileSampler, depth);
            //    l += SpecularTransmit(in ray, intersection, scene, tileSampler, depth);
            //}

            return l;
        }
    }
}