﻿using System;
using System.Numerics;
using Tracing.Core.Maths;

namespace Tracing.Core
{
    public class CameraSample
    {
        public Vector2 pFilm;
        public float time;
        public Vector2 pLens;
    }

    public class Camera
    {
        private const float PiOver4 = (float)(Math.PI/4);
        private const float PiOver2 = (float)(Math.PI/2);

        public Matrix4x4 CameraToScreen { get; }
        public Matrix4x4 ScreenToRaster { get; }
        public Matrix4x4 RasterToScreen;
        public Matrix4x4 RasterToCamera { get; }
        public Matrix4x4 CameraToWorld { get; }
        public Film Film { get; }

        public float lensRadius { get; set; }
        public float focalDistance { get; set; }

        static Matrix4x4 CreatePerspectiveFieldOfView(float fov, float n, float f)
        {
            Matrix4x4 persp = new Matrix4x4(
                1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, f / (f - n), 1,
                0, 0, -f * n / (f - n), 0);

            // Scale canonical perspective view to specified field of view
            float invTanAng = 1.0f / (float)Math.Tan(fov / 2);
            return Matrix4x4.CreateScale(invTanAng, invTanAng, 1) * persp;
        }

        public Camera(float lensRadius, float focalDistance, float fov)
        {
            CameraToScreen = CreatePerspectiveFieldOfView(fov, 1e-2f, 1000f);

            Vector3 filmres = new Vector3(Settings.Instance.Output.Width, Settings.Instance.Output.Height, 1);
            Bounds<Vector2> viewWindow = new Bounds<Vector2>(-Vector2.One, Vector2.One);

            Matrix4x4 s1 = Matrix4x4.CreateScale(filmres);
            Matrix4x4 s2 = Matrix4x4.CreateScale(1 / (viewWindow.Max.X - viewWindow.Min.X),
                1 / (viewWindow.Min.Y - viewWindow.Max.Y), 1);
            Matrix4x4 t3 = Matrix4x4.CreateTranslation(-viewWindow.Min.X, -viewWindow.Max.Y, 0);
            ScreenToRaster = t3 * s1 * s2;

            Matrix4x4.Invert(ScreenToRaster, out RasterToScreen);

            Matrix4x4.Invert(CameraToScreen, out Matrix4x4 screenToCam);
            RasterToCamera = RasterToScreen * screenToCam;

            CameraToWorld = Matrix4x4.Identity;
            this.lensRadius = lensRadius;
            this.focalDistance = focalDistance;

            var cropWindow = new Bounds<Vector2>(Vector2.Zero, Vector2.One);

            Film = new Film(Settings.Instance.Output, in cropWindow, new TriangleFilter(), 0.35f, "output.png");
        }

        public float GetRay(in CameraSample cameraSample, out Ray r)
        {
            Vector3 pFilm = new Vector3(cameraSample.pFilm.X, cameraSample.pFilm.Y, 0); // ????
            Vector3 pCamera = Vector3.Transform(pFilm, RasterToCamera);
            r = new Ray(new Vector3(0, 0, 0), Vector3.Normalize(pCamera));
            r.Origin = Vector3.Transform(r.Origin, CameraToWorld);
            r.Direction = Vector3.Transform(r.Direction, CameraToWorld);
            // Modify ray for depth of field
            if (lensRadius > 0)
            {
                // Sample point on lens
                Vector2 pLens = lensRadius * ConcentricSampleDisk(cameraSample.pLens);

                // Compute point on plane of focus
                float ft = focalDistance / r.Direction.Z;
                Vector3 pFocus = r.Transform(ft);

                // Update ray for effect of lens
                r.Origin = new Vector3(pLens.X, pLens.Y, 0);
                r.Direction = Vector3.Normalize(pFocus - r.Origin);
            }
            return 1.0f;
        }

        private Vector2 ConcentricSampleDisk(Vector2 u)
        {
            // Map uniform random numbers to $[-1,1]^2$
            Vector2 uOffset = 2f * u - Vector2.One;

            // Handle degeneracy at the origin
            if (uOffset.X == 0 && uOffset.Y == 0) return Vector2.Zero;

            // Apply concentric mapping to point
            float theta, r;
            if (Math.Abs(uOffset.X) > Math.Abs(uOffset.Y))
            {
                r = uOffset.X;
                theta = PiOver4 * (uOffset.Y / uOffset.X);
            }
            else
            {
                r = uOffset.Y;
                theta = PiOver2 - PiOver4 * (uOffset.X / uOffset.Y);
            }
            return r * new Vector2((float) Math.Cos(theta), (float) Math.Sin(theta));
        }
    }

    public class RNG
    {
        private const ulong PCG32_DEFAULT_STATE = 0x853c49e6748fea9b;
        private const ulong PCG32_MULT = 0x5851f42d4c957f2dUL;

        private ulong state = PCG32_DEFAULT_STATE;
        private ulong inc = 0xda3e39cb94b95bdb;

        public RNG()
        {
        }

        public RNG(ulong seed)
        {
            SetSequence(seed);
        }

        public float UniformFloat()
        {
            return Math.Min(ZeroTwoSequenceSampler.OneMinusEpsilon,
                UniformUInt32() * 2.3283064365386963e-10f);
        }

        public void SetSequence(ulong initseq)
        {
            state = 0u;
            inc = (initseq << 1) | 1u;
            UniformUInt32();
            state += PCG32_DEFAULT_STATE;
            UniformUInt32();
        }

        internal int UniformInt32()
        {
            unchecked
            {
                return (int)UniformUInt32();
            }
        }

        internal int UniformInt32(uint v)
        {
            unchecked
            {
                return (int) UniformUInt32(v);
            }
        }

        public uint UniformUInt32()
        {
            unchecked
            {
                ulong oldstate = state;
                state = oldstate * PCG32_MULT + inc;
                uint xorshifted = (uint) (((oldstate >> 18) ^ oldstate) >> 27);
                int rot = (int) (oldstate >> 59);
                return (xorshifted >> rot) | (xorshifted << ((~rot + 1) & 31));
            }
        }

        internal uint UniformUInt32(uint b)
        {
            unchecked
            {
                uint threshold = (uint) ((~b + 1L) % b);
                while (true)
                {
                    uint r = UniformUInt32();
                    if (r >= threshold) return r % b;
                }
            }
        }
    }
}