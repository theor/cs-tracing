﻿using System.ComponentModel;
using System.Drawing;

namespace Tracing.Core {
    public class Settings
    {
        private const float DegToRadians = 0.0174533f;

        public static Settings Instance;

        private static int _height = 2048;
        public Size Output { get; set; } = new Size(_height, _height);
        public bool Parallel { get; set; } = true;
        public bool ShowNormals { get; set; } = true;
        public int SamplesPerPixels { get; set; } = 16;
        public float LensRadius { get; set; } = 0.1f;

        public float FocalDistance { get; set; } = 30f;
        public float Fov { get; set; } = DegToRadians * 19.5f;
    }
}