﻿using System.Numerics;

namespace Tracing.Core.Maths {
    public struct Spectrum
    {
        public Vector3 RGB;
        public static Spectrum Zero = new Spectrum(Vector3.Zero);
        public Spectrum(Vector3 rgb)
        {
            RGB = rgb;
        }

        public bool IsBlack()
        {
            return RGB == Vector3.Zero;
        }

        public static Spectrum operator /(Spectrum a, float b)
        {
            return new Spectrum(a.RGB / b);
        }

        public static Spectrum operator *(Spectrum a, float b)
        {
            return new Spectrum(a.RGB * b);
        }

        public static Spectrum operator *(Spectrum a, Spectrum b)
        {
            return new Spectrum(a.RGB * b.RGB);
        }

        public static Spectrum operator +(Spectrum a, Spectrum b)
        {
            return new Spectrum(a.RGB + b.RGB);
        }
    }
    public struct Ray
    {
        public Vector3 Origin;
        public Vector3 Direction;

        public Ray(Vector3 origin, Vector3 direction)
        {
            Origin = origin;
            Direction = direction;
        }

        public override string ToString()
        {
            return $"{nameof(Origin)}: {Origin}, {nameof(Direction)}: {Direction}";
        }

        public Vector3 Transform(float distance)
        {
            return Origin + Direction * distance;
        }
    }
}