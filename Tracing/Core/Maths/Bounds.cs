﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Numerics;

namespace Tracing.Core.Maths {
    public struct Bounds<T>
    {
        public T Min;
        public T Max;

        public Bounds(T min, T max)
        {
            Min = min;
            Max = max;
        }

        public override string ToString()
        {
            return $"{nameof(Min)}: {Min}, {nameof(Max)}: {Max}";
        }

        public static Bounds<Point> Intersect(Bounds<Point> a, Bounds<Point> b)
        {
            int x1 = Math.Max(a.Min.X, b.Min.X);
            int x2 = Math.Min(a.Max.X, b.Max.X);
            int y1 = Math.Max(a.Min.Y, b.Min.Y);
            int y2 = Math.Min(a.Max.Y, b.Max.Y);

            if (x2 >= x1
                && y2 >= y1)
            {
                return new Bounds<Point>(new Point(x1,y1), new Point(x2, y2));
            }

            return new Bounds<Point>(Point.Empty, Point.Empty);
        }
    }

    public static class BoundsExtensions
    {
        public static Point ToPointCeiling(this Vector2 p)
        {
            return new Point((int) Math.Ceiling(p.X), (int) Math.Ceiling(p.Y));
        }
        public static Point ToPointFloor(this Vector2 p)
        {
            return new Point((int) Math.Floor(p.X), (int) Math.Floor(p.Y));
        }
        public static Vector2 ToVector2(this Point p)
        {
            return new Vector2(p.X, p.Y);
        }

        public static int Height(this Bounds<Point> b)
        {
            return b.Max.Y - b.Min.Y;
        }

        public static int Width(this Bounds<Point> b)
        {
            return b.Max.X - b.Min.X;
        }

        public static int Area(this Bounds<Point> b)
        {
            var diagonal = b.Diagonal();
            return diagonal.X * diagonal.Y;
        }
        public static IEnumerable<Point> EnumeratePixels(this Bounds<Point> b)
        {
            for (int j = b.Min.Y; j < b.Max.Y; j++)
            for (int i = b.Min.X; i < b.Max.X; i++)
            {
                yield return new Point(i,j);
            }
        }
        public static Bounds<Point> ToBounds(this Rectangle r)
        {
            return new Bounds<Point>(r.Location, Point.Add(r.Location, r.Size));
        }
        public static Point Diagonal(this Bounds<Point> b)
        {
            return new Point(b.Max.X - b.Min.X, b.Max.Y - b.Min.Y);
        }
    }
}