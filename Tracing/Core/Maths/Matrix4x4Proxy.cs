﻿using System;
using System.Linq;
using System.Numerics;

namespace Tracing.Core.Maths {
    public class Matrix4x4Proxy
    {
        private readonly Matrix4x4 _m;

        public Matrix4x4Proxy(Matrix4x4 m)
        {
            _m = m;
        }

        public string Text => String.Join("\r\n", Values.Select(row => String.Join(", ", row.Select(f => String.Format("{0,7:F3}", f)))));
        public string Easy => String.Join("\r\n", Values.Select(row => String.Join(", ", row.Select(f => Math.Abs(f) < Single.Epsilon ? "       " : String.Format("{0,7:F3}", f)))));

        public float[][] Values => new[]
        {
            new[]{ _m.M11, _m.M21,_m.M31,_m.M41 },
            new[]{ _m.M12, _m.M22,_m.M32,_m.M42 },
            new[]{ _m.M13, _m.M23,_m.M33,_m.M43 },
            new[]{ _m.M14, _m.M24,_m.M34,_m.M44 },
        };
    }
}