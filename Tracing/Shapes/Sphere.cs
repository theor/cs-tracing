﻿using System;
using System.Diagnostics;
using System.Numerics;
using Tracing.Core;
using Tracing.Core.Maths;

namespace Tracing.Shapes {
    class Plane : IObject
    {
        public Vector3 normal;
        public Vector3 center;

        public Plane(Vector3 normal, Vector3 center, Material material)
        {
            this.normal = normal;
            this.center = center;
            Material = material;
        }

        public bool Intersects(in Ray ray, out Intersection intersection)
        {
            float denom = Vector3.Dot(normal, ray.Direction);
            if (Math.Abs(denom) > 0.0001f) // your favorite epsilon
            {
                float t = Vector3.Dot(center - ray.Origin, normal) / denom;
                if (t >= 0)
                {
                    intersection = new Intersection
                    {
                        Object = this,
                        ContactNormal = normal,
                        ContactPoint = Vector3.Zero,
                        wo = -ray.Direction,
                        DistanceSquared = t*t,
                    };
                    return true; // you might want to allow an epsilon here too
                }
            }

            intersection = default;
            return false;
        }

        public Material Material { get; }
    }

    class Sphere : IObject
    {
        public Material Material { get; }
        private Vector3 _position;
        public float Radius;

        public ref Vector3 Position => ref _position;

        public Sphere(Vector3 vector3, float radius, Material material)
        {
            Material = material;
            _position = vector3;
            Radius = radius;
        }

        public bool Intersects(in Ray oray, out Intersection intersectionInfo)
        {
            //return PtIntersects(ref oray, out intersectionInfo);
            return PbrtIntersects(in oray, out intersectionInfo);
        }

        private bool PbrtIntersects(in Ray oray, out Intersection intersectionInfo)
        {
            intersectionInfo = default;

            Ray ray = oray;
            ray.Origin -= _position;

            // Compute quadratic sphere coefficients

            // Initialize _float_ ray coordinate values
            double ox = ray.Origin.X, oy = ray.Origin.Y, oz = ray.Origin.Z;
            double dx = ray.Direction.X, dy = ray.Direction.Y, dz = ray.Direction.Z;
            double a = dx * dx + dy * dy + dz * dz;
            double b = 2 * (dx * ox + dy * oy + dz * oz);
            double c = ox * ox + oy * oy + oz * oz - Radius * Radius;

            // Solve quadratic equation for _t_ values
            if (!Quadratic(a, b, c, out double t0, out double t1))
            {
                intersectionInfo = default;
                return false;
            }

            // Check quadric shape _t0_ and _t1_ for nearest intersection
            //if (t0 > ray.tMax || t1.LowerBound() <= 0) return false;
            double tShapeHit = t0;
            if (tShapeHit <= 0)
            {
                tShapeHit = t1;
                //    if (tShapeHit > ray.tMax) return false;
            }

            // Compute sphere hit position and $\phi$
            Vector3 pHit = ray.Transform((float) tShapeHit);

            // Refine sphere intersection point
            pHit *= Radius / pHit.Length();
            if (Math.Abs(pHit.X) < Single.Epsilon && Math.Abs(pHit.Y) < Single.Epsilon)
                pHit.X = 1e-5f * Radius;
            double phi = Math.Atan2(pHit.Y, pHit.X);
            if (phi < 0) phi += 2 * Math.PI;

            // Test sphere intersection against clipping parameters
            double phiMax = Math.PI * 2;
            float zMin = -Radius;
            float zMax = Radius;
            if (zMin > -Radius && pHit.Z < zMin || zMax < Radius && pHit.Z > zMax ||
                phi > phiMax)
            {
                if (Math.Abs(tShapeHit - t1) < Double.Epsilon)
                {
                    intersectionInfo = default;
                    return false;
                }
                //if (t1 > ray.tMax) return false;
                tShapeHit = t1;
                // Compute sphere hit position and $\phi$
                pHit = ray.Transform((float) tShapeHit);

                // Refine sphere intersection point
                pHit *= Radius / pHit.Length();
                if (Math.Abs(pHit.X) < Single.Epsilon && Math.Abs(pHit.Y) < Single.Epsilon)
                    pHit.X = 1e-5f * Radius;

                phi = Math.Atan2(pHit.Y, pHit.X);
                if (phi < 0)
                    phi += 2 * Math.PI;

                if (zMin > -Radius && pHit.Z < zMin ||
                    zMax < Radius && pHit.Z > zMax || phi > phiMax)
                {
                    intersectionInfo = default;
                    return false;
                }
            }

            intersectionInfo.DistanceSquared = (float) tShapeHit;
            intersectionInfo.ContactPoint = pHit - _position;
            intersectionInfo.ContactNormal = Vector3.Normalize(pHit);
            intersectionInfo.wo = -ray.Direction;

            intersectionInfo.Object = this;
            return true;
        }

        private bool Quadratic(double a, double b, double c, out double t0, out double t1)
        {
            // Find quadratic discriminant
            double discrim = b * b - 4.0 * a * c;
            if (discrim < 0.0)
            {
                t0 = 0;
                t1 = 0;
                return false;
            }
            double rootDiscrim = Math.Sqrt(discrim);

            double floatRootDiscrim = rootDiscrim;

            // Compute quadratic _t_ values
            double q;
            if ((float)b < 0)
                q = -0.5 * (b - floatRootDiscrim);
            else
                q = -0.5 * (b + floatRootDiscrim);
            t0 = q / a;
            t1 = c / q;
            if (t0 > t1)
            {
                var tmp = t0;
                t0 = t1;
                t1 = tmp;
            }
            return true;
        }

        public bool PtIntersects(ref Ray ray, out Intersection intersectionInfo)
        {
            intersectionInfo = default;
            // Calculate ray start's offset from the sphere center
            Vector3 p = ray.Origin - _position;

            float rSquared = Radius * Radius;
            float pD = Vector3.Dot(p, ray.Direction);

            // The sphere is behind or surrounding the start point.
            if (pD > 0 || Vector3.Dot(p, p) < rSquared)
            {
                return false;
            }

            // Flatten p into the plane passing through c perpendicular to the ray.
            // This gives the closest approach of the ray to the center.
            Vector3 a = p - pD * ray.Direction;

            float aSquared = Vector3.Dot(a, a);

            // Closest approach is outside the sphere.
            if (aSquared > rSquared)
            {
                return false;
            }

            // Calculate distance from plane where ray enters/exits the sphere.    
            float h = (float)Math.Sqrt(rSquared - aSquared);

            // Calculate intersection point relative to sphere center.
            Vector3 i = a - h * ray.Direction;

            Vector3 intersection = _position + i;
            Vector3 normal = i / Radius;
            // We've taken a shortcut here to avoid a second square root.
            // Note numerical errors can make the normal have length slightly different from 1.
            // If you need higher precision, you may need to perform a conventional normalization.
            intersectionInfo.DistanceSquared = (intersection - ray.Origin).LengthSquared();
            intersectionInfo.ContactPoint = intersection;
            Debug.Assert(Math.Abs(normal.LengthSquared() - 1) < 0.001);
            intersectionInfo.ContactNormal = normal;
            intersectionInfo.ContactNormal = Vector3.UnitY; // Vector3.Normalize(pHit - _position);
            intersectionInfo.wo = -ray.Direction;
            intersectionInfo.Object = this;
            return true;// (intersection, normal);
        }
    }
}