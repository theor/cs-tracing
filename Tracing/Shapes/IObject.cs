﻿using System.Numerics;
using Tracing.Core;
using Tracing.Core.Maths;

namespace Tracing.Shapes {
    public interface ILight {
        Spectrum Sample_Li(Intersection intersection, Vector2 get2D, out Vector3 wi, out float pdf, VisibilityTester visibility);
    }
    public interface IObject
    {
        bool Intersects(in Ray r, out Intersection intersection);
        Material Material { get; }
    }
}